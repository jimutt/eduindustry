﻿using System;
using System.IO;
using System.Reflection;
using ICities;
using UnityEngine;
using ColossalFramework.Plugins;
using ColossalFramework;

namespace eduIndustryMod
{


    public class eduIndustry: IUserMod
    {
        public void OnLoad()
        {

        }

        public void OnUnload()
        {
        }

        public string Name
        {
            get { return "eduIndustry"; }
        }

        public string Description
        {
            get { return "This mod makes education level the only requirement for industry upgrades. When the industry gets enough over-educated workers the industry will upgrade."; }
        }
    }

    public class IndustryLevelHandler : LevelUpExtensionBase
    {

        public const int LVL2_LIMIT = 25;
        public const int LVL3_LIMIT = 140;

        public Level curLvl;

        public override IndustrialLevelUp OnCalculateIndustrialLevelUp(IndustrialLevelUp levelUp, int averageEducation, int serviceScore, ushort buildingID, Service service, SubService subService, Level currentLevel)
        {

            Array16<Building> buildingsList = BuildingManager.instance.m_buildings;
            
            Building building = buildingsList.m_buffer[buildingID];

            int workerCount = building.m_citizenCount;

            int size = building.m_length * building.m_width;

            //Calculate number of workers required for first upgrade to prevent upgrades for buildings with only 1 employee:
            int requiredWorkers;
            if (size <= 2)
            {

                requiredWorkers = 2;

            }
            else if (size <= 4)
            {

                requiredWorkers = 3;

            }
            else
            {

                requiredWorkers = size - (size / 4);

            }


            curLvl = currentLevel;

            //Determin next level:
            levelUp.targetLevel = curLvl;

            if(curLvl == Level.Level1 && averageEducation >= LVL2_LIMIT && workerCount >= requiredWorkers) 
            {
                levelUp.targetLevel = Level.Level2;
            }

            else if(curLvl == Level.Level2 && averageEducation >= LVL3_LIMIT) 
            {
                levelUp.targetLevel = Level.Level3;
            }

            //Update progress bar:       
            levelUp.educationProgress = getLevelUpProgress(averageEducation);
            levelUp.serviceProgress = getLevelUpProgress(averageEducation);

            levelUp.tooFewServices = false;

            return levelUp;
            
        }

        public int getLevelUpProgress(float avgEdu)
        {
            float levelProgress = 0;

            if (curLvl == Level.Level1)
            {
                levelProgress = ((avgEdu / LVL2_LIMIT) * 1999);
            }

            else if (curLvl == Level.Level2)
            {
                levelProgress = (((avgEdu - LVL2_LIMIT) / (LVL3_LIMIT - LVL2_LIMIT)) * 1999);
            }

            if (levelProgress > 1999.0)
            {
                levelProgress = 1999;
            }

            return (int)levelProgress;
        }
    }
}